<?php

function acf_photo_gallery_image_fields( $args, $attachment_id, $field){
	return array(
		'url' => array(
			'type' => 'text', 
			'label' => 'URL', 
			'name' => 'url', 
			'value' => ($args['url'])?$args['url']:null
		),
		'target' => array(
			'type' => 'checkbox', 
			'label' => 'Abrir em uma nova aba', 
			'name' => 'target', 
			'value' => ($args['target'])?$args['target']:null
		),
		'title' => array(
			'type' => 'text', 
			'label' => 'Título', 
			'name' => 'title', 
			'value' => ($args['title'])?$args['title']:null
		),
		'caption' => array(
			'type' => 'textarea', 
			'label' => 'Subtítulo', 
			'name' => 'caption', 
			'value' => ($args['caption'])?$args['caption']:null
		)
	);
}
add_filter( 'acf_photo_gallery_image_fields', 'acf_photo_gallery_image_fields', 10, 3 );
