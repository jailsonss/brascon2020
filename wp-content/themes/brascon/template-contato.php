<?php
/**
 * The main template file
 *
 * This is the most generic template file in a WordPress theme
 * and one of the two required files for a theme (the other being style.css).
 * It is used to display a page when nothing more specific matches a query.
 * E.g., it puts together the home page when no home.php file exists.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package WordpressGulpBoilerplate
 */

/* Template name: Contato */
get_header();
?>
<div id="primary" class="content-area content-contato">
	<main id="main" class="site-main">

		<div class="modal fade" id="faleConosco" tabindex="-1" role="dialog" aria-labelledby="faleConoscoLabel" aria-hidden="true">
			<div class="modal-dialog" role="document">
				<div class="modal-content">
					<?php echo do_shortcode('[contact-form-7 id="333" title="Fale Conosco"]'); ?>
				</div>
			</div>
		</div>

		<div class="modal fade" id="trabalheConosco" tabindex="-1" role="dialog" aria-labelledby="trabalheConoscoLabel" aria-hidden="true">
			<div class="modal-dialog" role="document">
				<div class="modal-content">
					<?php echo do_shortcode('[contact-form-7 id="334" title="Trabalhe Conosco"]'); ?>
				</div>
			</div>
		</div>

		<div class="modal fade" id="fornecedor" tabindex="-1" role="dialog" aria-labelledby="fornecedorLabel" aria-hidden="true">
			<div class="modal-dialog" role="document">
				<div class="modal-content">
					<?php echo do_shortcode('[contact-form-7 id="335" title="Seja um fornecedor"]'); ?>
				</div>
			</div>
		</div>

		<div class="modal fade" id="terreno" tabindex="-1" role="dialog" aria-labelledby="terrenoLabel" aria-hidden="true">
			<div class="modal-dialog" role="document">
				<div class="modal-content">
					<?php echo do_shortcode('[contact-form-7 id="336" title="Ofereça seu terreno"]'); ?>
				</div>
			</div>
		</div>

		<div class="modal fade" id="vendas" tabindex="-1" role="dialog" aria-labelledby="vendasLabel" aria-hidden="true">
			<div class="modal-dialog" role="document">
				<div class="modal-content">
					<?php echo do_shortcode('[contact-form-7 id="4276" title="Interesse - Todos (Mobile)"]'); ?>
				</div>
			</div>
		</div>

		<div class="contato">

			<div class="container">
				<div class="row">
					<div class="col-md-6 animated fadeInLeft">
						<h2>Contatos</h2>
						<a href="#" class="button white email" data-toggle="modal" data-target="#faleConosco">Fale conosco</a>
						<a href="#" class="button white trabalhar" data-toggle="modal" data-target="#trabalheConosco">Trabalhe conosco</a>
						<a href="#" class="button white fornecedor" data-toggle="modal" data-target="#fornecedor">Seja um fornecedor</a>
						<a href="#" class="button white mapa" data-toggle="modal" data-target="#terreno">ofereça seu terreno</a>
						<a href="#" class="button white vendas d-sm-none" data-toggle="modal" data-target="#vendas">Vendas</a>
						<a href="#" class="button fone"><?php the_field('fone') ?></a><br>
						<a href="#" class="button mapa-alt"><?php the_field('endereco') ?></a>
					</div>
					<div class="col-md-6 animated fadeInRight d-none d-sm-block">
						<h2>Vendas</h2>
						<?php
						$whatsapp = get_field('whatsapp');
						?>
						<a href="https://api.whatsapp.com/send?phone=55<?php echo str_replace(' ','',$whatsapp); ?>" class="button whatsapp"><?php the_field('whatsapp') ?></a><br>
						<div class="contact-form">
							<h3>Tem interesse em um imóvel Brascon?</h3>
							<?php echo do_shortcode('[contact-form-7 id="332" title="Interesse - Todos"]'); ?>
						</div>
					</div>
				</div>
			</div>

		</div>

		<?php get_template_part( 'template-parts/newsletter' ) ?>

	</main><!-- #main -->
</div><!-- #primary -->
<?php
get_footer();
