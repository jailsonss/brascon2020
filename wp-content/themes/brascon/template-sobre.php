<?php
/**
 * The main template file
 *
 * This is the most generic template file in a WordPress theme
 * and one of the two required files for a theme (the other being style.css).
 * It is used to display a page when nothing more specific matches a query.
 * E.g., it puts together the home page when no home.php file exists.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package WordpressGulpBoilerplate
 */

/* Template name: Sobre */
get_header();
?>
<div id="primary" class="content-area content-sobre">
	<main id="main" class="site-main">

		<section id="hero" style="background-image: url(<?php echo get_the_post_thumbnail_url()?>)">
			
			<div class="hero-content container animated fadeInUp">
				<?php 
				$video = get_field('video_sobre');
				if ( $video ) { 
					echo '<a data-fancybox href="'.$video.'" class="video-play white"><span>Veja o vídeo</span></a>';
				}?>
				<div class="row">
					<div class="col-sm-9">
						<h3><?php the_field('titulo_secao_1') ?></h3>
						<h4><?php the_field('subtitulo_secao_1') ?></h4>
					</div>
					<div class="col-sm-3">
						<img src="<?php echo get_template_directory_uri() ?>/images/branding/brascon-icon.png" class="brascon-icon">
					</div>
				</div>
				<div class="row">
					<div class="col-sm-4">
						<h3><?php the_field('titulo_coluna_1') ?></h3>
						<p><?php the_field('texto_coluna_1') ?></p>
					</div>
					<div class="col-sm-4">
						<h3><?php the_field('titulo_coluna_2') ?></h3>
						<p><?php the_field('texto_coluna_2') ?></p>
					</div>
					<div class="col-sm-4">
						<h3><?php the_field('titulo_coluna_3') ?></h3>
						<p><?php the_field('texto_coluna_3') ?></p>
					</div>
				</div>
			</div>

		</section>

		<section id="politica" style="background-image: url(<?php the_field('imagem_secao_2') ?>)">

			<div class="container">
				<div class="row">
					<div class="col-sm-6 col-md-5">
						<div class="content wow fadeInUp">
							<h2><?php the_field('titulo_secao_2') ?></h2>
							<?php the_field('texto_secao_2') ?>
						</div>
					</div>
				</div>
			</div>
			
		</section>

		<section id="diferenciais" style="background-image: url(<?php the_field('imagem_secao_3') ?>)">

			<div class="container">
				<div class="row justify-content-end">
					<div class="col-sm-6 col-md-5">
						<div class="content wow fadeInUp">
							<h2><?php the_field('titulo_secao_3') ?></h2>
							<?php the_field('texto_secao_3') ?>
						</div>
					</div>
				</div>
			</div>
			
		</section>

		<?php
		$texto_portfolio = get_field('texto_portfolio');
		$portfolio = new WP_Query( array( 
			'post_type' => 'imovel',
			'tax_query'      => array(
				array(
					'taxonomy' => 'status',
					'terms' => array('portfolio'),
					'field' => 'slug'
				)
			),
			'posts_per_page' => -1 ) );
			if( $portfolio->have_posts() ): ?>

				<section id="portfolio">

					<div class="owl-container">

						<div class="owl-gallery owl-carousel" id="owlPortfolio">

							<?php while ( $portfolio->have_posts() ) : $portfolio->the_post(); ?>

								<a href="<?php the_permalink() ?>" data-dot="<button role='button'><span><?php the_title() ?></span></button>">
									<div class="item" style="background-image: url(<?php echo get_the_post_thumbnail_url()?>)"></div>
								</a>

							<?php endwhile; ?>

						</div>

						<div class="container">

							<div class="row">
								<div class="col-sm-6 col-md-5">

									<div class="content wow fadeInUp">
										<h2>PORTFÓLIO BRASCON</h2>
										<p><?php echo $texto_portfolio; ?></p>
										<a href="../imoveis">Veja também: <span>IMÓVEIS À VENDA</span></a>
									</div>

								</div>
								<div class="col-sm-6 col-md-7">

									<?php while ( $portfolio->have_posts() ) : $portfolio->the_post(); ?>

										<a class="full-url" href="<?php the_permalink() ?>"></a>

									<?php endwhile;
									wp_reset_query();
									?>

									<div class="navigation">
										<div class="navigation-arrows wow fadeIn"><div class="navigation-dots"></div></div>
									</div>

								</div>
							</div>
						</div>

					</div>

				</section>


			<?php endif; ?>

			<?php get_template_part( 'template-parts/newsletter' ) ?>

		</main><!-- #main -->
	</div><!-- #primary -->
	<?php
	get_footer();
