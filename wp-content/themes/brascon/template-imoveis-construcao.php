<?php
/**
 * The main template file
 *
 * This is the most generic template file in a WordPress theme
 * and one of the two required files for a theme (the other being style.css).
 * It is used to display a page when nothing more specific matches a query.
 * E.g., it puts together the home page when no home.php file exists.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package WordpressGulpBoilerplate
 */

/* Template name: Imóveis - Em construção */
get_header();
?>
<div id="primary" class="content-area content-imoveis">
	<main id="main" class="site-main">

		<section id="hero">

			<div class="container">

				<?php 
				$imoveis = new WP_Query( array( 
					'post_type' => 'imovel',
					'tax_query'      => array(
						array(
							'taxonomy' => 'status',
							'terms' => array('em-construcao'),
							'field' => 'slug'
						)
					),
					'posts_per_page' => -1 ) );

				if($imoveis->have_posts()) :

					while ( $imoveis->have_posts() ) :  $imoveis->the_post(); ?>

						<div class="imovel wow fadeInUp">
							<div class="imovel-header">
								<h3><?php the_field('slogan') ?></h3>
								<?php 
								$images = acf_photo_gallery( 'galeria' , get_the_ID() );
								if ( is_array($images) || is_object($images) ) : ?>

									<div class="owl-container">

										<div class="navigation">
											<div class="navigation-arrows"><div class="navigation-dots"></div></div>
										</div>
										<div class="owl-gallery owl-carousel" id="imovelGallery">

											<?php foreach( $images as $image ): ?>

												<a href="<?php echo $image['full_image_url'] ?>" data-fancybox="<?php the_ID(); ?>" data-caption="<?php echo $image['caption'] ?>" data-dot="<button role='button'><span></span></button>">
													<div class="thumb" style="background-image: url(<?php echo acf_photo_gallery_resize_image($image['full_image_url'],720,420) ?>)"></div>
												</a>

											<?php endforeach; ?>

										</div>
									</div>

								<?php endif; ?>
							</div>
							<a href="<?php echo get_permalink(); ?>">
								<div class="details">
									<div class="col-left">
										<div class="detail local">Miramar</div>
										<div class="detail area"><?php the_field('area') ?>m²</div>
										<div class="detail quartos"><?php the_field('quartos') ?> quarto<?php if(get_field('quartos') > 1) { echo 's'; } ?> <?php if( get_field('suites') ) { echo ' &nbsp; | &nbsp; ' . get_field('suites') . ' suíte';  } ?><?php if(get_field('suites') > 1) { echo 's'; } ?></div>
										<?php $logo = get_field('logo_branca'); if($logo) : ?>
										<img class="logo" src="<?php echo $logo ?>">
									<?php endif; ?>
								</div>
							</div>
						</a>
					</div>

					<?php wp_reset_postdata(); endwhile; else : ?>

					<div class="row">
						<div class="col-md-8">
							<h2 class="wow fadeInRight">Em construção</h2>
							<img src="<?php echo get_template_directory_uri() ?>/images/icons/icn-construcao.png" class="icn-construcao top wow fadeInRight">
							<h4 class="wow fadeInLeft">No momento não estamos com nenhum imóvel em construção, mas você pode conhecer nossos outros imóveis ou se cadastrar no formulário abaixo e lhe avisaremos assim que iniciarmos uma nova construção.</h4>
							<a href="<?php echo get_site_url(); ?>/lancamentos" class="button white wow fadeInUp">Conheça nossos lançamentos</a>
						</div>
						<div class="col-md-4">
							<img src="<?php echo get_template_directory_uri() ?>/images/icons/icn-construcao.png" class="icn-construcao side wow fadeInRight">
						</div>
					</div>

				<?php endif; ?>

			</div>

		</section>

		<?php get_template_part( 'template-parts/interesse' ) ?>

	</main><!-- #main -->
</div><!-- #primary -->
<?php
get_footer();
