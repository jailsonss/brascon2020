<?php
/**
 * The template for displaying the footer
 *
 * Contains the closing of the #content div and all content after.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package WordpressGulpBoilerplate
 */

?>

</div><!-- #content -->

<?php
$contato = get_page_by_path( 'contato' );
$facebook = get_field('facebook',$contato->ID);
$instagram = get_field('instagram',$contato->ID);
$linkedin = get_field('linkedin',$contato->ID);
$youtube = get_field('youtube',$contato->ID);
?>

<div class="modal fade" id="obrigado" tabindex="-1" role="dialog" aria-labelledby="obrigadoLabel" aria-hidden="true">
	<div class="modal-dialog" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<h4 class="modal-title" id="obrigadoLabel">Muito obrigado!</h4>
			</div>
			<div class="modal-body">
				<p>Recebemos ses dados e vamos entrar em contato com você o quanto antes.</p>
			</div>
			<div class="modal-footer">
				<button type="button" class="button white" data-dismiss="modal">Fechar</button>
			</div>
		</div>
	</div>
</div>

<footer id="colophon" class="site-footer animated fadeInUp">
	<div class="site-info">
		<div class="address"><a href="#"><?php echo get_field('endereco',$contato->ID) ?></a></div>
		<ul class="social">
			<?php if ($facebook) : ?>
				<li><a href="<?php echo $facebook ?>" target="_blank"><span class="facebook"></span></a></li>
			<?php endif; ?>
			<?php if ($instagram) : ?>
				<li><a href="<?php echo $instagram ?>" target="_blank"><span class="instagram"></span></a></li>
			<?php endif; ?>
			<?php if ($linkedin) : ?>
				<li><a href="<?php $linkedin ?>" target="_blank"><span class="linkedin"></span></a></li>
			<?php endif; ?>
			<?php if ($youtube) : ?>
				<li><a href="<?php echo $youtube ?>" target="_blank"><span class="youtube"></span></a></li>
			<?php endif; ?>
		</ul>
		<div class="fones">
			<?php $whatsapp = get_field('whatsapp',$contato->ID); ?>
			<span class="tel"><a href="tel:<?php echo get_field('fone',$contato->ID) ?>"><?php echo get_field('fone',$contato->ID) ?></a></span>
			<span class="vendas"><a href="https://api.whatsapp.com/send?phone=55<?php echo str_replace(' ','',$whatsapp); ?>">Vendas: <?php echo $whatsapp ?></a></span>
		</div>
		<div class="chat"><a href="#">FALAR COM CORRETOR</a></div>
	</div><!-- .site-info -->
</footer><!-- #colophon -->
</div><!-- #page -->

<?php wp_footer(); ?>

</body>
</html>
