<?php
/**
 * The template for displaying archive pages
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package WordpressGulpBoilerplate
 */

get_header(); ?>

	<div id="primary" class="content-area content-blog invert">
	<main id="main" class="site-main">

		<div class="container">

			<header class="page-header">
				<form action="<?php echo home_url( '/' ); ?>" method="get" class="dark full-width">
					<div class="form-group">
						<label for="nome">Buscar</label>
						<input type="text" name="s" class="form-control" id="search" placeholder="Buscar" value="<?php the_search_query(); ?>">
						<input type="image" alt="Search" src="<?php echo get_template_directory_uri() ?>/images/icons/icn-search.png">
					</div>
				</form>
			</header>

			<div id="loop-options" data-page="<?php echo $page ?>" data-cat=""></div>

			<div id="loop-content">

				<?php
				if ( have_posts() ) :

					echo '<div class="card-columns">';

					while ( have_posts() ) : the_post();

						get_template_part( 'template-parts/content-article-loop' );

					endwhile;

					echo '</div>';

					numeric_posts_nav();

				else :

					get_template_part( 'template-parts/content', 'none' );

				endif; ?>

			</div>

		</div>

	</main><!-- #main -->

	<?php get_template_part( 'template-parts/newsletter' ) ?>

</div><!-- #primary -->

<?php
get_footer();
