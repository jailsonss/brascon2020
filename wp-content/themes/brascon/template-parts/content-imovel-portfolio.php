<div id="primary" class="content-area content-single content-single-portfolio">
	<main id="main" class="site-main">

		<section id="hero" style="background-image: url(<?php echo get_the_post_thumbnail_url()?>)">

			<div class="img-hero d-sm-none" style="background-image: url(<?php echo get_the_post_thumbnail_url()?>)"></div>

			<div class="container">
				<div class="row">
					<div class="col-md-5 content animated fadeInRight">
						<h3><?php the_title() ?></h3>
						<h6 class="bairro"><?php $bairro = get_field('bairro'); echo $bairro->name ?></h6>
						<div class="entrega"><h6>Entregue em <span><?php the_field('ano_de_entrega') ?></span></h6></div>
						<div class="vendido"><h2>100%<br class="d-none d-sm-block">Vendido</h2></div>
					</div>
				</div>
			</div>
			
		</section>

		<?php 
		$images = acf_photo_gallery( 'galeria' , get_the_ID() );
		if ( is_array($images) || is_object($images) ) : ?>
			<section id="galeria">

				<div class="owl-container">
					<div class="container">
						<h2>Fotos</h2>
					</div>
					<div class="navigation">
						<div class="navigation-arrows"><div class="navigation-dots"></div></div>
					</div>
					<div class="owl-gallery owl-carousel" id="imovelGallery">

						<?php foreach( $images as $image ): ?>

							<a href="<?php echo $image['full_image_url'] ?>" data-fancybox="galeria" data-caption="<?php echo $image['caption'] ?>" data-dot="<button role='button'><span><?php echo $image['caption'] ?></span></button>">
								<div class="thumb" style="background-image: url(<?php echo acf_photo_gallery_resize_image($image['full_image_url'],720,420) ?>)"></div>
							</a>

						<?php endforeach; ?>

					</div>
				</div>

			</section>
		<?php endif; ?>



		<?php get_template_part( 'template-parts/interesse' ) ?>

	</main>
</div>