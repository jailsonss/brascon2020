<div class="row">
	<div class="col-md-6 col-lg-5">
		<div class="form-group">
			<label for="nome">Seu nome</label>
			[text* nome id:nome class:form-control placeholder "Seu nome"]
		</div>
	</div>
	<div class="col-md-6 col-lg-5">
		<div class="form-group">
			<label for="email">E-mail</label>
			[text* email id:email class:form-control placeholder "E-mail"]
		</div>
	</div>
	<div class="col-lg-2">
		[submit class:button class:white "Enviar"]
	</div>
</div>