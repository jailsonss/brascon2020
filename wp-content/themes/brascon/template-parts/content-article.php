<article id="<?php the_ID() ?>" class="post-<?php the_ID() ?>">
	
	<div class="container">

		<section id="hero" style="background-image: url(<?php echo get_the_post_thumbnail_url()?>);">
			<?php 
			$video = get_field('post_video');
			if ( $video ) { 
				echo '<a data-fancybox href="'.$video.'" class="video-play"><span>Veja o vídeo</span></a>';
			}?>
			
		</section>

		<section id="content">
			
			<div class="article-body">
				<div class="article-meta">
					<span class="date caption"><?php wpgb_posted_on() ?></span>
					<?php $categories = get_the_category();
					if ( ! empty( $categories ) ) {
						echo '<span class="tag">'.$categories[0]->name.'</span>';
					} ?>
				</div>
				<h3><?php echo the_title() ?></h3>
				<?php the_content(); ?>

				<?php 
				$images = acf_photo_gallery( 'post_gallery' , get_the_ID() );
				if ( !empty($images) ) : ?>
					
					<div class="owl-container">
						<div class="navigation">
							<div class="navigation-arrows"><div class="navigation-dots"></div></div>
						</div>
						<div class="owl-gallery owl-carousel" id="postGallery">

							<?php foreach( $images as $image ): ?>
								
								<a href="<?php echo $image['full_image_url'] ?>" data-fancybox="post-<?php the_ID() ?>" data-caption="<?php echo $image['caption'] ?>" data-dot="<button role='button'><span><?php echo $image['caption'] ?></span></button>">
									<div class="thumb" style="background-image: url(<?php echo acf_photo_gallery_resize_image($image['full_image_url'],720,420) ?>)"></div>
								</a>

							<?php endforeach; ?>

						</div>
					</div>

				<?php endif; ?>

				<a href="http://www.facebook.com/sharer.php?u=<?php echo the_permalink(); ?>&t=<?php echo get_the_title(); ?>" class="button black facebook" target="_blank" rel="noopener noreferrer">Compartilhar</a>

			</div>

			<div class="sidebar">
				<h3>Talvez lhe interesse:</h3>

				<div class="related-articles">

					<?php
					$articles = get_field('related_articles'); 

					if ( is_array($articles) || is_object($articles) ) :
						foreach ( $articles as $article ) : ?>

							<article>
								<a href="<?php echo get_permalink($article->ID) ?>">
									<?php echo get_the_post_thumbnail( $article->ID, 'thumbnail', array( 'class' => 'thumb' ) ); ?>
									<h4><?php echo $article->post_title ?></h4>
									<p class="caption"><?php echo get_the_date('',$article->ID) ?></p>
								</a>
							</article>

							<?php
						endforeach;

					else: 

						$articles = get_posts( array( 
							'post__not_in' => array(get_the_ID()),
							'post_type' => 'post',
							'tax_query'  => array(
								array(
									'taxonomy'  => 'category',
									'field'     => 'slug',
									'terms'     =>  array($categories[0]->name),
								),
							)
							, 'posts_per_page' => 3 ) );

							foreach ( $articles as $article ) : ?>

								<article>
									<a href="<?php echo get_permalink($article->ID) ?>">
										<?php echo get_the_post_thumbnail( $article->ID, 'thumbnail', array( 'class' => 'thumb' ) ); ?>
										<h4><?php echo $article->post_title ?></h4>
										<p class="caption"><?php echo get_the_date('',$article->ID) ?></p>
									</a>
								</article>

								<?php
							endforeach;

						endif; ?>

					</div>
				</div>

			</section>

		</div>

	</article>