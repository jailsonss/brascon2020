<?php
/**
 * Template part for displaying a message that posts cannot be found
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package WordpressGulpBoilerplate
 */

?>

<section class="no-results not-found">
	<div class="page-content">
		<h2>Sem resultados.</h2>
	</div><!-- .page-content -->
</section><!-- .no-results -->
