<?php $page_imoveis = get_page_by_path( 'imoveis' ); ?>
<article class="card after">
	<a href="#"  data-toggle="modal" data-target="#faleConosco">
		<h3><?php echo get_field('titulo_cta',$page_imoveis->ID) ?></h3>
		<p><?php echo get_field('texto_cta',$page_imoveis->ID) ?></p>
	</a>
</article>