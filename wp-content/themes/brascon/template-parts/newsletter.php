<section id="template-newsletter">
	<div class="container">
		<h3 class="wow fadeInUp">Receba nossas novidades no seu email</h3>
		<?php echo do_shortcode('[contact-form-7 id="338" title="Newsletter"]'); ?>
		<a href="#" class="qualitare"></a>
	</div>
</section>