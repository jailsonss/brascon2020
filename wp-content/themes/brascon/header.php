<?php
/**
 * The header for our theme
 *
 * This is the template that displays all of the <head> section and everything up until <div id="content">
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package WordpressGulpBoilerplate
 */

?>
<!doctype html>
<html <?php language_attributes(); ?>>
<head>
	<meta charset="<?php bloginfo( 'charset' ); ?>">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="profile" href="http://gmpg.org/xfn/11">
	<link rel="shortcut icon" href="<?php echo get_template_directory_uri(); ?>/images/branding/favicon.png">

	<script type="text/javascript">
		var stylesheet_directory_uri = "<?php echo get_stylesheet_directory_uri(); ?>";
	</script>

	<?php wp_head(); ?>
</head>

<body <?php body_class(); ?>>
	<div id="page" class="site">

		<header id="masthead" class="site-header animated fadeInDown">
			<div class="site-branding">
				<a href="<?php echo esc_url( home_url( '/' ) ); ?>" rel="home"><div class="logo"></div></a>
			</div><!-- .site-branding -->

			<nav id="site-navigation" class="main-navigation">
				<div class="menu-toggle"></div>
				<?php
				wp_nav_menu( array(
					'theme_location'  => 'primary',
				    'depth'           => 2, // 1 = no dropdowns, 2 = with dropdowns.
				    'container'       => 'div',
				    'container_id'    => 'main-menu',
				    'menu_class'      => 'navbar-nav mr-auto',
				    'fallback_cb'     => 'WP_Bootstrap_Navwalker::fallback',
				    'walker'          => new WP_Bootstrap_Navwalker(),
				) );
				?>
				<?php
				$contato = get_page_by_path( 'contato' );
				$facebook = get_field('facebook',$contato->ID);
				$instagram = get_field('instagram',$contato->ID);
				$linkedin = get_field('linkedin',$contato->ID);
				$youtube = get_field('youtube',$contato->ID);
				?>
				<ul class="social">
					<?php if ($facebook) : ?>
						<li><a href="<?php echo $facebook ?>" target="_blank"><span class="facebook"></span></a></li>
					<?php endif; ?>
					<?php if ($instagram) : ?>
						<li><a href="<?php echo $instagram ?>" target="_blank"><span class="instagram"></span></a></li>
					<?php endif; ?>
					<?php if ($linkedin) : ?>
						<li><a href="<?php $linkedin ?>" target="_blank"><span class="linkedin"></span></a></li>
					<?php endif; ?>
					<?php if ($youtube) : ?>
						<li><a href="<?php echo $youtube ?>" target="_blank"><span class="youtube"></span></a></li>
					<?php endif; ?>
				</ul>
			</nav><!-- #site-navigation -->
		</header><!-- #masthead -->

		<div id="content" class="site-content">

			<?php if(!is_front_page()): ?>
				<div class="container breadcrumbs-wrap">
					<div class="animated fadeInLeft"><?php the_breadcrumbs(); ?></div>
					<div class="animated fadeInLeft"><div class="back_to_home"><a href="<?php echo esc_url( home_url( '/' ) ); ?>">Voltar para Início</a></div></div>
				</div>
			<?php endif; ?>
