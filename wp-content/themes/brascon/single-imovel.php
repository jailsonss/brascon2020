<?php
/**
 * The template for displaying all single imovel
 *
 * @package WordpressGulpBoilerplate
 */

get_header(); 

while ( have_posts() ) : the_post();

	$id = get_the_ID();
	$pageviews = get_field('pageviews');
	$intencao = get_field('status');

	update_post_meta( $id , 'pageviews' , $pageviews+1 , $pageviews);

	switch ($intencao->slug) {
		case 'portfolio':
		get_template_part( 'template-parts/content-imovel-portfolio' );
		break;
		case 'avulso':
		get_template_part( 'template-parts/content-imovel-avulso' );
		break;
		case 'aluguel':
		get_template_part( 'template-parts/content-imovel-avulso' );
		break;
		default:
		get_template_part( 'template-parts/content-imovel' );
		break;
	}


endwhile;

get_footer();
